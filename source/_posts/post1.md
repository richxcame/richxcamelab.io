---
title: Blok hakynda
date: '2021-01-24 07:39:00'
cover: /depo/post1Cover.svg
thumbnail: /depo/post1Thumbnail.svg
tags: Hexo
categories: Web

---

### Blogy nädip ýasamaly?

Bloga hoş geldiňiz! Blogy ýasamak üçin **Hexo** ulandym we ony **Gitlabda** saklaýan.(şonuň üçin hem .gitlab.io) Siz hem özüňiziň blogyňyzy ýasamak isleýän bolsaňyz, Hexo gowy SSG (*Static Site Generator*). Bulardan başga-da Jekyll, Ghost... hem ulanyp bileriňiz. Hexo-da öz isleýän temamy tapdym (Icarus). Saýty iň ýakyn wagtda *Icarus* tema geçirip, saýty doly işleýän hala getirmekçi...

Soraglaryňyz bar bolsa <baygeldicolukov@gmail.com>-a ýazyp bilersiňiz.
